function Error = back_prop(nw, A, y)

    Error = cell(length(nw.layers));
    Error{1} = 0;
    Error{length(nw.layers)} = A{length(nw.layers)} - y';
    
    if length(nw.layers) > 2
        for l = (length(nw.layers) - 1) : -1 : 2
            Error{l} = (nw.weights{l}' * Error{l+1}) .* (A{l} .* (1 - A{l}));
            Error{l} = Error{l}(2:end);
        end
    end
    
end