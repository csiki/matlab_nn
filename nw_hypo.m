% y oszlopvektor

function y = nw_hypo(nw, x)
    
    a = [1;x'];
    for l = 2:length(nw.layers)
        z = nw.weights{l - 1} * a;
        a = [1; sigmf(z, [1 0])];
    end
    
    y = a(2:end);
    
end