% init_nw: network struct
% X: bemenetek m�trixa (soronk�nt)
% Y: kimenetek m�trixa (soronk�nt)

function nw = train_nw(init_nw, X_, Y_)
    
    nwtmp = init_nw;
    lambda = 0.001;
    options = optimoptions(@fminunc,'GradObj','on');
    optweights = fminunc(@cost_function, matrices_to_vector(init_nw.weights), options);
    
    nw = init_nw;
    nw.weights = vector_to_matrices(optweights, nw.layers);
    
    function [J, grad] = cost_function(weights)
        
        nwtmp.weights = vector_to_matrices(weights, nwtmp.layers); % weight matrices
        [m,~] = size(X_);
        
        % bemenet shuffle, hogy ne tanuljon r�
        input = [X_, Y_];
        input = input( randperm(size(input, 1)), : );
        X = input(:, 1:size(X_, 2));
        Y = input(:, (size(X_, 2) + 1) : (size(X_, 2) + size(Y_, 2)));
        
        % cost kisz�m�t�sa
        J = 0;
        for i = 1:m
            for k = 1:nwtmp.layers(end)
                hypo_res = nw_hypo(nwtmp, X(i,:));
                J = J + Y(i, k) * log(hypo_res(k)) + (1 - Y(i, k)) * log(1 - hypo_res(k));
            end
        end
        J = J * -(1/m);
        
        % s�lyok korl�toz�sa
        sum = 0;
        for l = 1 : length(nwtmp.weights)
            [sl, slp1] = size(nwtmp.weights{l});
            tmpmatrix = nwtmp.weights{l};
            for i = 1 : sl
                for j = 1 : slp1
                    sum = sum + tmpmatrix(i, j) ^ 2;
                end
            end
        end
        sum = sum * (lambda / (2*m));
        
        J = J + sum;
        
        % grad kisz�m�t�sa: forward-, backpropagation
        % Delta felt�lt�se
        Delta = cell(1, length(nwtmp.layers) - 1);
        D = cell(1, length(nwtmp.layers) - 1);
        for l = 1 : (length(nwtmp.layers) - 1)
            Delta{l} = zeros(size(nwtmp.weights{l}));
            D{l} = zeros(size(nwtmp.weights{l}));
        end
        
        % forward �s back prop
        for i = 1:m
            A = forward_prop(nwtmp, X(i,:));
            Error = back_prop(nwtmp, A, Y(i,:));

            for l = 1 : (length(nwtmp.layers) - 1)
                Delta{l} = Delta{l} + Error{l+1} * A{l}';
            end
        end

        % grad kisz�m�t�sa Delt�b�l
        % + korl�toz�s
        for l = 1 : (length(nwtmp.layers) - 1)
            for i = 1 : nwtmp.layers(l + 1)
                D{l}(i,1) = (1/m) * Delta{l}(i,1);
                for j = 2 : (nwtmp.layers(l) + 1) % j=2-t�l korl�tozunk, bias-t nem
                    D{l}(i,j) = (1/m) * Delta{l}(i,j) + lambda * nwtmp.weights{l}(i,j);
                end
            end
        end
        
        grad = matrices_to_vector(D);
        
        % grad checking
%         epsilon = 0.0001;
%         grad_approx = zeros(1, length(grad));
%         for i = 1:length(grad)
%             wplus = weights;
%             wplus(i) = wplus(i) + epsilon;
%             wminus = weights;
%             wminus(i) = wminus(i) - epsilon;
%             
%             nwtmp2 = neural_network(nwtmp.layers, wplus);
%             grad_approx(i) = cost_for_grad_check(nwtmp2, X, Y);
%             
%             nwtmp2 = neural_network(nwtmp.layers, wminus);
%             grad_approx(i) = grad_approx(i) - cost_for_grad_check(nwtmp2, X, Y);
%             grad_approx(i) = grad_approx(i) / (2*epsilon);
%         end
%         
%         disp('grad:');
%         disp(grad);
%         disp('approx:');
%         disp(grad_approx);
        
    end
end

function J = cost_for_grad_check(nwtmp, X, Y)
    
    [m,~] = size(X);
    
    % cost kisz�m�t�sa
    J = 0;
    for i = 1:m
        for k = 1:nwtmp.layers(end)
            hypo_res = nw_hypo(nwtmp, X(i,:));
            J = J + Y(i, k) * log(hypo_res(k)) + (1 - Y(i, k)) * log(1 - hypo_res(k));
        end
    end
    J = J * -(1/m);
    
    % s�lyok korl�toz�sa
    lambda = 0.001;
    sum = 0;
    for l = 1 : length(nwtmp.weights)
        [sl, slp1] = size(nwtmp.weights{l});
        tmpmatrix = nwtmp.weights{l};
        for i = 1 : sl
            for j = 1 : slp1
                sum = sum + tmpmatrix(i, j) ^ 2;
            end
        end
    end
    sum = sum * (lambda / (2*m));
    
    J = J + sum;
end