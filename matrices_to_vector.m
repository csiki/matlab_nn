function weights_vec = matrices_to_vector(matrices)
    
    % sum vector m�rete
    vec_size = 0;
    for i = 1:length(matrices)
        vec_size = vec_size + numel(matrices{i});
    end
    
    weights_vec = zeros(1, vec_size);
    pos = 1;
    for i = 1:length(matrices)
        weights_vec(pos  : (pos + numel(matrices{i}) - 1)) = matrices{i}(:);
        pos = pos + numel(matrices{i});
    end

end