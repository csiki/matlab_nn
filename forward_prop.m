function A = forward_prop(nw, x)
    
    % hozzuk l�tre az aktiv�ci�s �rt�ket tartalmaz� cella t�mb�t
    % az egyes sorvektorok egy r�teg aktic�vi�s �rt�keit tartalmazz�k
    A = cell(1, length(nw.layers));
    Z = cell(1, length(nw.layers));
    A{1} = [1;x'];
    Z{1} = 0;
    for l = 2 : length(nw.layers)
        Z{l} = nw.weights{l - 1} * A{l - 1};
        A{l} = [1; sigmf(Z{l}, [1 0])];
    end
    
    A{length(nw.layers)} = A{length(nw.layers)}(2:end); % 1-est kivessz�k az elej�r�l
end