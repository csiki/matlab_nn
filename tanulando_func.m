function [p1, p2] = tanulando_func(x1, x2)
    if (x1*x2 > x2^1.5)
        p1 = 1;
        p2 = 0;
    else
        p1 = 0;
        p2 = 1;
    end
end