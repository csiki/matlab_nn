% visszat�r�si �rt�ke egy neural network struct:
% nw.weights
%   - (theta �rt�kek), a h�l�zat s�lyai
%   - m�trixok cell t�mbj�ben t�rolva
%   - mx egy sora egy node aktivit�s�nak kisz�mol�s�ra
% nw.layers
%   - rejtett neuronok sz�ma r�tegenk�nt (vektor)
%   - els� eleme az inputok sz�ma
%   - utols� eleme az outputok sz�ma
% opcion�lis bemenete a m�r kisz�molt s�lyok sorvektora

function nw = neural_network(layers, varargin)
    
    % r�tegek init
    nw.layers = layers;
    
    % s�lyok init
    if isempty(varargin)
        for i = 1 : (length(layers) - 1)
            nw.weights{i} = rand(layers(i + 1), layers(i) + 1) * 2 - 1;
        end
    else
        % sz�tszedj�k a vektort
        weights_vec = varargin{1};
        marginsum = 1; 
        for i = 1 : (length(layers) - 1)
            nw.weights{i} = reshape(weights_vec(marginsum : (marginsum + layers(i+1)*(layers(i)+1) - 1)), layers(i+1), layers(i)+1);
            marginsum = marginsum + layers(i+1)*(layers(i)+1);
        end
    end
    
end